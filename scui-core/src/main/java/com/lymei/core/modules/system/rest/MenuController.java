package com.lymei.core.modules.system.rest;

import com.lymei.common.po.ResultResponse;
import com.lymei.common.utils.RequestUtils;
import com.lymei.core.modules.system.service.IMenuService;
import com.lymei.core.modules.system.service.IOnlineService;
import com.lymei.lib.vo.system.MenuVo;
import com.lymei.lib.vo.system.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author lymei
 * @title: MenuController
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2416:48
 */
@RestController
@RequestMapping("/system/menu/")
public class MenuController {

    @Autowired
    private IMenuService menuService;

    @Autowired
    private IOnlineService onlineService;

    private static final String[] PERMISSIONS = new String[]{"list.add","list.edit","list.delete","user.add","user.edit","user.delete"};

    @GetMapping(value = {"/my/1.4.1"})
    public ResultResponse selMyMenu(){
        UserVo user = onlineService.get(RequestUtils.getHttpServletRequest());
        List<MenuVo> menus = menuService.selectMenuByUserId(Optional.ofNullable(user).map(UserVo::getId).orElse(null));
        Map<String, Object> result = new HashMap<>();
        result.put("menu",menus);
        result.put("permissions",PERMISSIONS);
        return ResultResponse.success(result);
    }

    @GetMapping("/list")
    public ResultResponse selMenuList(){
        List<MenuVo> menus = menuService.selectList();

        Map<String, Object> result = new HashMap<>();
        result.put("menu",menus);
        result.put("permissions",PERMISSIONS);
        return ResultResponse.success(result);
    }

}
