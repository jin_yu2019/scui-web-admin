package com.lymei.core.modules.system.mapper;

import com.lymei.common.mapper.IBaseMapper;
import com.lymei.lib.po.system.Meta;
import com.lymei.lib.qc.MetaQueryCriteria;
import com.lymei.lib.vo.system.MetaVo;

/**
 * @author lymei
 * @title: IMetaMapper
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2611:27
 */
public interface IMetaMapper extends IBaseMapper<Meta> {

    MetaVo selectMetaByMenuId(MetaQueryCriteria queryCriteria);
}
