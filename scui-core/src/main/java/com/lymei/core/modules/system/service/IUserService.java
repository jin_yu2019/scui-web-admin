package com.lymei.core.modules.system.service;

import com.lymei.common.service.IBaseService;
import com.lymei.lib.po.system.User;
import com.lymei.lib.vo.system.UserVo;

/**
 * @author lymei
 * @title: IUserService
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2116:13
 */
public interface IUserService extends IBaseService<User> {

    /**
     * 创建用户
     * @param userVo
     * @return
     */
    UserVo create(UserVo userVo);

}
