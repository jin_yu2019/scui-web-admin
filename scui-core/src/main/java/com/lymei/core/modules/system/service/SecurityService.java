package com.lymei.core.modules.system.service;

import com.lymei.common.exception.BusinessException;
import com.lymei.lib.po.system.UserDetailsImpl;
import com.lymei.lib.vo.system.UserVo;
import lombok.var;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * @author lymei
 * @title: SecurityService
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2410:38
 */
public class SecurityService {

    public static UserVo getUser(){
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null){
            throw new BusinessException("当前登录状态已过期");
        }
        if (authentication.getPrincipal() instanceof UserDetailsImpl){
            return ((UserDetailsImpl)authentication.getPrincipal()).getUser();
        }
        throw new BusinessException("找不到当前登录的信息");
    }

    public static String getUserName(){
        return getUser().getUserName();
    }

    public static Long getUserId(){
        return getUser().getId();
    }
}
