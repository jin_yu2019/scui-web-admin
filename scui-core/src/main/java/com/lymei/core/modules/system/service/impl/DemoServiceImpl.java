package com.lymei.core.modules.system.service.impl;

import com.lymei.core.modules.system.service.IDemoService;
import com.lymei.lib.properties.AppProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author lymei
 * @title: DemoServiceImpl
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2615:33
 */
@Service
public class DemoServiceImpl implements IDemoService {

    @Autowired
    private AppProperties appProperties;

    @Override
    public String getVer() {
        return appProperties.getVersion();
    }
}
