package com.lymei.core.modules.system.rest;

import com.lymei.common.po.ResultResponse;
import com.lymei.core.modules.system.service.IUserService;
import com.lymei.lib.vo.system.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author lymei
 * @title: UserController
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2116:11
 */
@RestController
@RequestMapping("/user")
public class UserController {


    @Autowired
    private IUserService userService;

    @PostMapping("/register")
    public ResultResponse create(@RequestBody UserVo user){
        return ResultResponse.success(userService.create(user));
    }
}
