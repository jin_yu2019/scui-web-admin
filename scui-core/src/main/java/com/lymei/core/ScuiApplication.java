package com.lymei.core;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;


/**
 * @author lymei
 * @title: ScuiApplication
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2010:15
 */
@SpringBootApplication
@EntityScan(basePackages = "com.lymei.lib.po")
@ComponentScan(basePackages = "com.lymei.**")
@MapperScan(basePackages = "com.lymei.core.modules.*.mapper.**")
@ConfigurationPropertiesScan("com.lymei")
public class ScuiApplication {
    public static void main(String[] args) {
        SpringApplication.run(ScuiApplication.class,args);
    }
}
