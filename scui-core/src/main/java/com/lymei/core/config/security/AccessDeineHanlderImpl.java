package com.lymei.core.config.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lymei.common.enums.Result;
import com.lymei.common.po.ResultResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author lymei
 * @title: 已通过认证用户无资源访问权限
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/2/910:29
 */
@Slf4j
public class AccessDeineHanlderImpl implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        log.warn(accessDeniedException.getMessage());
        response.setStatus(Result.FAIL.getCode());
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json; charset=UTF-8");
        response.getWriter().print(new ObjectMapper().writeValueAsString(ResultResponse.fail("无此操作权限，请联系管理员")));
    }
}
