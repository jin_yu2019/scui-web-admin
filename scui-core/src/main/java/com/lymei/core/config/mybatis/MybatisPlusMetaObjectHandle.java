package com.lymei.core.config.mybatis;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.lymei.core.modules.system.service.SecurityService;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @author lymei
 * @title: MybatisPlusMetaObjectHandle
 * @projectName scui-admin
 * @description: TODO mybatis-plus 自动填充功能
 * @date 2022/1/2010:45
 */
@Component
public class MybatisPlusMetaObjectHandle implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
//        this.strictInsertFill(metaObject,"creater", ()->SecurityService.getUserId(),Long.class);
//        this.strictInsertFill(metaObject,"createName",()->SecurityService.getUserName(),String.class);
//        this.strictInsertFill(metaObject,"createTime", ()->LocalDateTime.now(),LocalDateTime.class);
//
//        this.strictInsertFill(metaObject,"updater",()->SecurityService.getUserId(),Long.class);
//        this.strictInsertFill(metaObject,"updateName",()->SecurityService.getUserName(),String.class);
//        this.strictInsertFill(metaObject,"updateTime",()->LocalDateTime.now(),LocalDateTime.class);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
//        this.strictUpdateFill(metaObject,"updater",()->SecurityService.getUserId(),Long.class);
//        this.strictUpdateFill(metaObject,"updateName",()->SecurityService.getUserName(),String.class);
//        this.strictUpdateFill(metaObject,"updateTime",()->LocalDateTime.now(),LocalDateTime.class);

    }
}
