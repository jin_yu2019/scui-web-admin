package com.lymei.core.config.exception;


import com.lymei.common.exception.BusinessException;
import com.lymei.common.po.ResultResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;

import java.nio.file.AccessDeniedException;

/**
 * @author lymei
 * @title: GlobalExceptionHandler
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2011:46
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    /**
     * 未登录
     * @return
     */
    @ExceptionHandler(HttpClientErrorException.Unauthorized.class)
    public ResultResponse NoLogin(Exception e){
        log.warn(e.getMessage());
        return ResultResponse.fail("请先登录");
    }


    /**
     * 无权限
     * @param e
     * @return
     */
    @ExceptionHandler(AccessDeniedException.class)
    public ResultResponse accessDeniedException(Exception e){
        log.warn(e.getMessage());
        return ResultResponse.fail("暂无权限");
    }


    /**
     * 未知异常
     * @param e
     * @return
     */
    @ExceptionHandler(RuntimeException.class)
    public ResultResponse runTimeException(Exception e){
        log.warn(e.getMessage());
        return ResultResponse.exception(e);
    }

    /**
     * 自定义的业务异常
     * @param e
     * @return
     */
    @ExceptionHandler(BusinessException.class)
    public ResultResponse businessException(Exception e){
        log.warn(e.getMessage());
        return ResultResponse.fail(e.getMessage());
    }

    /**
     * 用户名或密码错误
     * @param e
     * @return
     */
    @ExceptionHandler(BadCredentialsException.class)
    public ResultResponse badCredentialsException(Exception e){
        log.warn(e.getMessage());
        return ResultResponse.fail("用户名或密码错误");
    }
}
