package com.lymei.common.enums;

/**
 * @author lymei
 * @title: Result
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2011:49
 */
public enum Result {

    SUCCESS(200,"成功"),
    FAIL(400,"失败"),
    ERROR(500,"不可预知的错误"),
    UNAUTHORIZED(401,"未鉴权")
    ;


    private final Integer code;

    private final String message;

    Result(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
