package com.lymei.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author lymei
 * @title: Status
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2010:30
 */
public enum Status {

    ENABLE(0,"启用"),
    DISABLE(1,"禁用")
    ;

    @EnumValue
    private final Integer code;
    @JsonValue
    private final String desc;

    Status(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
