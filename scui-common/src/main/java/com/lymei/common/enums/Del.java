package com.lymei.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author lymei
 * @title: Del
 * @projectName scui-admin
 * @description:
 * @date 2022/1/2010:24
 */
public enum Del {

    E(0,"未删除"),
    N(1,"已删除")
    ;

    @EnumValue
    private final Integer code;
    @JsonValue
    private final String desc;

    Del(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
