package com.lymei.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author lymei
 * @title: BaseMapper
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2016:11
 */
public interface IBaseMapper<T> extends BaseMapper<T> {
}
