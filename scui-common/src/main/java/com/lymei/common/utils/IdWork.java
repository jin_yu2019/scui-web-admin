package com.lymei.common.utils;

import com.baomidou.mybatisplus.core.incrementer.DefaultIdentifierGenerator;
import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import org.springframework.stereotype.Component;

/**
 * @author lymei
 * @title: IdWork
 * @projectName scui-admin
 * @description: 主键生成器
 * @date 2022/1/2010:35
 */
@Component
public class IdWork implements IdentifierGenerator {

    @Deprecated
    @Override
    public Number nextId(Object entity) {
        DefaultIdentifierGenerator identifierGenerator = new DefaultIdentifierGenerator();
        return identifierGenerator.nextId(entity);
    }

    public Long nextId(){
        return this.nextId(null).longValue();
    }

}
