package com.lymei.common.po;

import com.lymei.common.enums.Result;
import lombok.Data;
import lombok.var;

import java.io.Serializable;
import java.sql.ResultSet;

/**
 * @author lymei
 * @title: ResultResponse
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2011:47
 */
@Data
public class ResultResponse implements Serializable {

    private boolean success;

    private Integer code;

    private Object data;

    private String message;

    private String error;


    private ResultResponse(Result result) {
        if (Result.SUCCESS.equals(result)){
            this.success = true;
        }else{
            this.success = false;
        }
        this.code = result.getCode();
        this.message = result.getMessage();
    }


    public static ResultResponse success(){
        return new ResultResponse(Result.SUCCESS);
    }

    public static ResultResponse success(Object t){
        var resp = new ResultResponse(Result.SUCCESS);
        resp.setData(t);
        return resp;
    }

    public static ResultResponse fail(Result result){
        return new ResultResponse(result);
    }

    public static ResultResponse fail(){
        var resp = new ResultResponse(Result.FAIL);
        return resp;
    }

    public static ResultResponse fail(String message){
        var resp = new ResultResponse(Result.FAIL);
        resp.setMessage(message);
        return resp;
    }

    public static ResultResponse exception(Exception exception){
        var resp = new ResultResponse(Result.ERROR);
        resp.setError(exception.getMessage());
        return resp;
    }



}
