package com.lymei.common.exception;

import com.lymei.common.enums.Result;

import lombok.Getter;

/**
 * @author lymei
 * @title: BusinessException
 * @projectName scui-admin
 * @description:
 * @date 2022/1/2014:37
 */
@Getter
public class BusinessException extends RuntimeException{

    public BusinessException(Result result) {
        super(result.getMessage());
    }

    public BusinessException(Exception e){
        super(e);
    }

    public BusinessException(String message) {
        super(message);
    }
}
