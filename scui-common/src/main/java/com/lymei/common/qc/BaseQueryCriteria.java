package com.lymei.common.qc;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author lymei
 * @title: BaseQueryCriteria
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2311:44
 */
@Data
public class BaseQueryCriteria implements Serializable {

    /**
     * 数据主键
     */
    private  Long id;

    /**
     * 起始日期
     */
    private LocalDate startDate;

    /**
     * 终止日期
     */
    private LocalDate endDate;

}
