package com.lymei.lib.qc;

import com.lymei.common.qc.BaseQueryCriteria;
import com.lymei.lib.po.system.RoleMenuRelation;
import com.lymei.lib.vo.system.RoleMenuRelationVo;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author lymei
 * @title: MenuQueryCriteria
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2311:46
 */
@Data
@NoArgsConstructor
public class MenuQueryCriteria extends BaseQueryCriteria {

    private Long pid;

    private Long userId;


}
