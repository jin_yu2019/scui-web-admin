package com.lymei.lib.vo.system;

import com.lymei.lib.po.system.RoleMenuRelation;
import lombok.Data;

/**
 * @author lymei
 * @title: RoleMenuRelationVo
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2311:59
 */
@Data
public class RoleMenuRelationVo extends RoleMenuRelation {
}
