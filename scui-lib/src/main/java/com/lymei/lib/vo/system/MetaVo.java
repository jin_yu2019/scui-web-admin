package com.lymei.lib.vo.system;

import com.lymei.lib.po.system.Meta;
import lombok.Data;

/**
 * @author lymei
 * @title: MetaVo
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2611:13
 */
@Data
public class MetaVo extends Meta {
}
