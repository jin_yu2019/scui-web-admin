package com.lymei.lib.vo.system;

import com.lymei.lib.po.system.User;
import lombok.Data;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * @author lymei
 * @title: UserVo
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2011:09
 */
@Data
public class UserVo extends User {

    private List<String> role;


    private String dashboard = "0";

}
