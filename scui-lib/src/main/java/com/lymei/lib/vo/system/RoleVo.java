package com.lymei.lib.vo.system;

import com.lymei.lib.po.system.Role;
import lombok.Data;

import java.util.LinkedHashSet;

/**
 * @author lymei
 * @title: RoleVo
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2114:33
 */
@Data
public class RoleVo extends Role {

}
