package com.lymei.lib.properties;

import lombok.Data;

/**
 * @author lymei
 * @title: OnlineProperties
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/2/98:51
 */
@Data
public class OnlineProperties {

    private String tokenHeaderName = "Authorization";

    private String tokenPrefix = "Bearer";

    private long tokenExpire = 30*60L;

    private String onlineKeyPrefix = "Online_";


}
