package com.lymei.lib.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author lymei
 * @title: AppProperties
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/2/98:47
 */
@ConfigurationProperties(prefix = "app")
@Data
public class AppProperties {

    private String version = "1.0.0";

    private OnlineProperties online;
}
