package com.lymei.lib.po.system;

import lombok.Data;

import java.io.Serializable;

/**
 * @author lymei
 * @title: LonginUser
 * @projectName scui-admin
 * @description:
 * @date 2022/1/2010:47
 */
@Data
public class LoginUser implements Serializable {

    private String username;

    private String password;

    private Long corpId;
}
