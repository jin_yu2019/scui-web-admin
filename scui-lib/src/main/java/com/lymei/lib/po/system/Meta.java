package com.lymei.lib.po.system;

import com.baomidou.mybatisplus.annotation.TableName;
import com.lymei.common.po.BasePo;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author lymei
 * @title: Meta
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2611:10
 */
@Data
@Entity
@Table(name = "sys_meta")
@TableName("sys_meta")
public class Meta extends BasePo {

    private Long menuId;

    private String title;

    private String icon;

    private String type;

    private Boolean affix;

    private Boolean hidden;

    private Boolean hiddenBreadcrumb;

    private String active;

    private Boolean fullpage;



}
