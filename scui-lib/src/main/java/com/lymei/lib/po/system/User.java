package com.lymei.lib.po.system;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lymei.common.enums.Status;
import com.lymei.common.po.BasePo;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author lymei
 * @title: User
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2010:49
 */
@Data
@Entity
@Table(name = "sys_user")
@TableName(value = "sys_user")
public class User extends BasePo {

    private String account;

    private String userName;

    @JsonIgnore
    private String password;

    private Status status;
}
