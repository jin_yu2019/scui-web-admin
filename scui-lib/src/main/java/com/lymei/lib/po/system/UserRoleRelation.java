package com.lymei.lib.po.system;

import com.baomidou.mybatisplus.annotation.TableName;
import com.lymei.common.po.BasePo;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author lymei
 * @title: UserRoleRelation
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2117:18
 */
@Data
@Entity
@Table(name = "sys_users_roles")
@TableName(value = "sys_users_roles")
public class UserRoleRelation extends BasePo {

    private Long userId;

    private Long roleId;
}
