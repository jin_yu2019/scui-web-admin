package com.lymei.lib.po.system;

import com.lymei.lib.vo.system.UserVo;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.LinkedHashSet;

/**
 * @author lymei
 * @title: UserDetailsImpl
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2015:41
 */
@Data
public class UserDetailsImpl implements UserDetails {

    public UserDetailsImpl(UserVo user) {
        this.user = user;
    }

    private UserVo user;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return new LinkedHashSet<>();
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getAccount();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
